plugins {
    id("com.android.library")
    id("org.jetbrains.kotlin.android")
    id("kotlin-kapt")
    `maven-publish`

}


android {
    namespace = "com.example.file_viewer"
    compileSdk = 34

    defaultConfig {
        minSdk = 24

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        consumerProguardFiles("consumer-rules.pro")
    }

    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_17
        targetCompatibility = JavaVersion.VERSION_17
    }
    kotlinOptions {
        jvmTarget = "17"
    }
    publishing {
        singleVariant("release") {
            withSourcesJar()
        }
    }
    buildFeatures{
        dataBinding = true
        viewBinding = true
    }
}

dependencies {

    implementation("androidx.core:core-ktx:1.12.0")
    implementation("androidx.appcompat:appcompat:1.6.1")
    implementation("com.google.android.material:material:1.11.0")
    implementation("androidx.constraintlayout:constraintlayout:2.1.4")

    implementation ("org.imaginativeworld.whynotimagecarousel:whynotimagecarousel:2.1.0")

    implementation ("me.relex:circleindicator:2.1.6")
    implementation ("com.google.android.exoplayer:exoplayer:2.19.1")

    implementation ("com.intuit.sdp:sdp-android:1.1.0")
    implementation ("com.github.bumptech.glide:glide:4.16.0")
    implementation ("com.github.piasy:BigImageViewer:1.8.1")
    implementation ("com.github.piasy:GlideImageLoader:1.8.1")
    implementation("com.otaliastudios:zoomlayout:1.9.0")
    implementation ("org.ocpsoft.prettytime:prettytime:4.0.5.Final")

    implementation ("com.github.Toxa2033:ScaleAndSwipeDismissImageView:v0.7")
}



afterEvaluate {
    publishing {
        publications {
            register("release", MavenPublication::class) {
                groupId = "com.taymay"
                artifactId = "file_viewer"
                version = "1.0.0"
                from(components["release"])
            }

        }
    }
}
