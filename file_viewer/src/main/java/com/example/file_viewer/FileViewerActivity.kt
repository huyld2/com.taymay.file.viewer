package com.example.file_viewer

import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.FileProvider
import androidx.core.view.setPadding
import androidx.databinding.adapters.ViewBindingAdapter.setPadding
import androidx.recyclerview.widget.RecyclerView
import androidx.viewbinding.ViewBinding
import com.example.file_viewer.databinding.ActivityAllFileViewerBinding
import com.example.file_viewer.databinding.ItemViewPagerBinding
import com.google.android.exoplayer2.ExoPlayer
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import org.imaginativeworld.whynotimagecarousel.listener.CarouselListener
import org.imaginativeworld.whynotimagecarousel.listener.CarouselOnScrollListener
import org.imaginativeworld.whynotimagecarousel.model.CarouselGravity
import org.imaginativeworld.whynotimagecarousel.model.CarouselItem
import org.imaginativeworld.whynotimagecarousel.utils.setImage
import java.io.File


class FileViewerActivity : AppCompatActivity() {
    private lateinit var allFileViewerBinding: ActivityAllFileViewerBinding
    private var exoPlayer: ExoPlayer? = null
    private var playbackPosition = 0L
    private var playWhenReady = true
    private var currentPosition = 0

    companion object {
        var pos = 0
        var listFile = mutableListOf<File>()
        var onFileViewer = fun(fileViewerActivity: FileViewerActivity,
                               llTopBanner: LinearLayout,
                               llBottomBanner: LinearLayout) {}

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        allFileViewerBinding = ActivityAllFileViewerBinding.inflate(layoutInflater)
        setContentView(allFileViewerBinding.root)
        Log.e("listssss", allFileViewerBinding.toString())
        onFileViewer(
            this,
            allFileViewerBinding.llTopBanner,
            allFileViewerBinding.llBottomBanner
        )
        mapExten()
        allFileViewerBinding.icBack.setOnClickListener { onBackPressed() }
        if (listFile.size == 1) allFileViewerBinding.tvTitle.text = "1 File"
        else allFileViewerBinding.tvTitle.text = listFile.size.toString() + " Files"
        Log.e("list", listFile.toString())
        val list = mutableListOf<CarouselItem>()
        for (item in listFile)
            list.add(CarouselItem(item.absolutePath))
        // C
        allFileViewerBinding.imCarousel.registerLifecycle(lifecycle)

        allFileViewerBinding.imCarousel.carouselGravity=CarouselGravity.CENTER

        allFileViewerBinding.imCarousel.carouselListener = object : CarouselListener {

            override fun onCreateViewHolder(
                layoutInflater: LayoutInflater, parent: ViewGroup
            ): ViewBinding {
                return ItemViewPagerBinding.inflate(
                    layoutInflater, parent, false
                )
            }


            override fun onBindViewHolder(
                binding: ViewBinding,
                item: CarouselItem,
                position: Int
            ) {
                val itemViewBinding = binding as ItemViewPagerBinding
                Log.e("position",position.toString())
                Log.e("currentPosition",allFileViewerBinding.imCarousel.currentPosition.toString())
                if (File(item.imageUrl!!).isVideo()) {
                    itemViewBinding.llInfo.visibility = View.GONE
                    itemViewBinding.imvPreview.visibility = View.VISIBLE
                    itemViewBinding.icPlayer.visibility = View.VISIBLE

                    itemViewBinding.imvPreview.apply {
                        scaleType = ImageView.ScaleType.FIT_CENTER

                        setImage(item, R.drawable.ic_wb_cloudy_with_padding)
                    }
                    itemViewBinding.root.setOnClickListener {
                        Log.e("isFullScreen", it.toString())
                        val intent = Intent(this@FileViewerActivity, MediaViewerActivity::class.java)
                        intent.putExtra("type", "Video")
                        intent.putExtra("path", item.imageUrl)

                        startActivity(intent)
                    }


                } else if (File(item.imageUrl!!).isPhoto()) {
                    itemViewBinding.imvPreview.visibility = View.VISIBLE
                    itemViewBinding.icPlayer.visibility = View.GONE
                    itemViewBinding.llInfo.visibility = View.GONE
                    itemViewBinding.imvPreview.apply {
                        scaleType = ImageView.ScaleType.FIT_CENTER

                        setImage(item, R.drawable.ic_wb_cloudy_with_padding)
                    }
                    itemViewBinding.imvPreview.setOnClickListener {
                        val intent = Intent(this@FileViewerActivity, MediaViewerActivity::class.java)
                        intent.putExtra("type", "Photo")
                        intent.putExtra("path", item.imageUrl)
                        startActivity(intent)
                    }
                } else {
                    itemViewBinding.llInfo.visibility = View.VISIBLE
                    itemViewBinding.imvPreview.visibility = View.GONE
                    itemViewBinding.icPlayer.visibility = View.GONE
                    var file = File(item.imageUrl!!)
                    itemViewBinding.tvName.text = file.name
                    itemViewBinding.tvSize.text = formatFileSize(file.length())
                    itemViewBinding.tvPath.text = file.absolutePath
                    itemViewBinding.tvTime.text = file.getPretyTime()
                    itemViewBinding.btnOpenWith.setOnClickListener {
                        openFile(file)
                    }
                }

            }
        }

        allFileViewerBinding.imCarousel.onScrollListener = object : CarouselOnScrollListener {

            override fun onScrollStateChanged(
                recyclerView: RecyclerView,
                newState: Int,
                position: Int,
                carouselItem: CarouselItem?
            ) {

            }
            @SuppressLint("SetTextI18n")
            override fun onScrolled(
                recyclerView: RecyclerView,
                dx: Int,
                dy: Int,
                position: Int,
                carouselItem: CarouselItem?
            ) {
                allFileViewerBinding.tvTitle.text = (allFileViewerBinding.imCarousel.currentVirtualPosition+1).toString() +" / "+listFile.size.toString() + " Files"
            }
        }


        Log.e("currentPosition",allFileViewerBinding.imCarousel.currentVirtualPosition.toString())
        allFileViewerBinding.imCarousel.setData(list)
        allFileViewerBinding.imCarousel.currentPosition = pos

    }



    private fun mapExten() {
        video_exts.forEach {
            videoMaps[it] = true
        }
        image_exts.forEach {
            imageMaps[it] = true
        }
        audio_exts.forEach { audioMaps[it] = true }
        document_exts.forEach { documentMaps[it] = true }
        archives_exts.forEach { archiveMaps[it] = true }

    }

    private fun releasePlayer() {
        exoPlayer?.let { player ->
            playbackPosition = player.currentPosition
            playWhenReady = player.playWhenReady
            player.release()
            exoPlayer = null
        }
    }

    override fun onStop() {
        super.onStop()
        releasePlayer()
    }

    override fun onPause() {
        super.onPause()
        releasePlayer()
    }

    override fun onDestroy() {
        super.onDestroy()
        releasePlayer()
    }

    private fun openFile(fileView: File) {
//        var d = requireContext().showLoading("Opening...")
//        d.show()
        this.runOnUiThread {
            val f_cp = File(
                (this.externalCacheDir?.absolutePath
                    ?: Environment.getExternalStorageDirectory().absolutePath) + "/.viewer/",
                fileView.name
            )
            f_cp.parentFile.mkdirs()
            val file = fileView.copyTo(
                f_cp, true
            )
            MainScope().launch {
                try {
                    val uri: Uri =
                        FileProvider.getUriForFile(
                            this@FileViewerActivity,
                            this@FileViewerActivity.packageName.toString() + ".provider",
                            file
                        )
                    val mime: String? = this@FileViewerActivity.contentResolver.getType(uri)
//                    d.dis()
                    val intent = Intent()
                    intent.action = Intent.ACTION_VIEW
                    intent.setDataAndType(uri, mime)
                    intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                    startActivity(intent)
                } catch (e: Exception) {
                }
            }
        }
    }

}