package com.example.file_viewer

import android.content.pm.ActivityInfo
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.WindowInsetsController
import android.widget.ImageButton
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.setPadding
import androidx.databinding.adapters.ViewBindingAdapter
import com.example.file_viewer.databinding.ActivityMediaViewerBinding
import com.google.android.exoplayer2.ExoPlayer
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import org.imaginativeworld.whynotimagecarousel.model.CarouselItem
import org.imaginativeworld.whynotimagecarousel.utils.setImage

class MediaViewerActivity : AppCompatActivity() {
    lateinit var mediaViewerBinding: ActivityMediaViewerBinding
    private var exoPlayer: ExoPlayer? = null
    private var playbackPosition = 0L
    private var playWhenReady = true
    private var isFullscreen = false
    private var defaultScreenOrientation = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mediaViewerBinding = ActivityMediaViewerBinding.inflate(layoutInflater)
        setContentView(mediaViewerBinding.root)
        mediaViewerBinding.spvFullView.setShowNextButton(false)
        mediaViewerBinding.spvFullView.setShowPreviousButton(false)

        when (intent.getStringExtra("type")) {
            "Video" -> {
                mediaViewerBinding.spvFullView.visibility = View.VISIBLE
                mediaViewerBinding.icBack.visibility = View.VISIBLE

                mediaViewerBinding.imvZoom.visibility = View.GONE
                exoPlayer = ExoPlayer.Builder(this).build()
                exoPlayer!!.playWhenReady = true
                Log.e("exoPlayer", exoPlayer.toString())
                mediaViewerBinding.spvFullView.player = this.exoPlayer
                when (requestedOrientation) {
                    ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED -> {

                        mediaViewerBinding.spvFullView.findViewById<ImageButton>(com.google.android.exoplayer2.ui.R.id.exo_fullscreen)
                            .apply {
                                setImage(CarouselItem(R.drawable.fullscreen_icon))
                                setPadding(30)
                            }
                    }

                    ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE -> {
                        mediaViewerBinding.spvFullView.findViewById<ImageButton>(com.google.android.exoplayer2.ui.R.id.exo_fullscreen)
                            .apply {
                                setImage(CarouselItem(R.drawable.exit_fullscreen_icon))
                                setPadding(30)
                            }
                    }
                }
                mediaViewerBinding.spvFullView.setFullscreenButtonClickListener { isFullscreen ->
                    Log.e("isFullscreen", isFullscreen.toString())


                    when (requestedOrientation) {
                        ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED -> {
                            enterFullscreen()
                        }

                        ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE -> {
                            exitFullscreen()
                        }
                    }

                }

                val mediaItem = MediaItem.fromUri(Uri.parse(intent.getStringExtra("path")))
                val mediaSource = ProgressiveMediaSource.Factory(DefaultDataSourceFactory(this))
                    .createMediaSource(mediaItem)
                exoPlayer?.apply {
                    setMediaSource(mediaSource)
                    seekTo(playbackPosition)
                    playWhenReady = playWhenReady
                    prepare()
                }

            }

            "Photo" -> {
                mediaViewerBinding.spvFullView.visibility = View.GONE
                mediaViewerBinding.imvZoom.visibility = View.VISIBLE
                mediaViewerBinding.icBack.visibility = View.VISIBLE

                mediaViewerBinding.imvZoom.apply {
                    scaleType = ImageView.ScaleType.CENTER_INSIDE

                    setImage(
                        CarouselItem(intent.getStringExtra("path")),
                        R.drawable.ic_wb_cloudy_with_padding
                    )
                }


            }
        }
        mediaViewerBinding.icBack.setOnClickListener {
            onBackPressed()
        }

    }

    private fun releasePlayer() {
        exoPlayer?.let { player ->
            playbackPosition = player.currentPosition
            playWhenReady = player.playWhenReady
            player.release()
            exoPlayer = null
        }
    }

    override fun onStop() {
        super.onStop()
        releasePlayer()
    }

    override fun onPause() {
        super.onPause()
        releasePlayer()
    }

    override fun onDestroy() {
        super.onDestroy()
        releasePlayer()
    }

    private fun enterFullscreen() {



        mediaViewerBinding.spvFullView.layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT
        mediaViewerBinding.spvFullView.layoutParams.height = ViewGroup.LayoutParams.MATCH_PARENT
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE
    }

    private fun exitFullscreen() {
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED
    }


}