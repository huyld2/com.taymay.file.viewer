package com.example.file_viewer

import android.content.Context
import android.content.Intent
import android.util.Log
import android.widget.LinearLayout
import org.ocpsoft.prettytime.PrettyTime
import java.io.File
import java.util.Date

var video_exts = arrayOf(
    ".mp4", ".avi", ".mov", ".wmv", ".flv"
)


fun Context.openFilesViewer(
    listFile: MutableList<File>,
    pos : Int,
    onFileViewer: (activity: FileViewerActivity, llTopBanner: LinearLayout, llBottomBanner: LinearLayout) -> Unit
) {
    FileViewerActivity.listFile = listFile
    FileViewerActivity.onFileViewer = onFileViewer
    FileViewerActivity.pos = pos
    Log.e("listssss", FileViewerActivity.listFile.toString())
    startActivity(
        Intent(this, FileViewerActivity::class.java)
    )
}

var image_exts = arrayOf(
    ".jpeg",
    ".jpg",
    ".png",
    ".gif",
    ".bmp",

    )
var audio_exts = arrayOf(".mp3", ".wav", ".flac", ".acc", ".ogg")
var document_exts = arrayOf(
    ".ppt", ".pptx", ".doc", ".docx", ".txt", ".pdf", ".xls", ".xlsx"
)
var archives_exts = arrayOf(
    ".zip", ".7z", ".tar.gz", ".tar", ".rar"
)
var apk_exts = arrayOf(
    "apk"
)

fun formatFileSize(bytes: Long): String {
    val kilobyte = 1024
    val megabyte = kilobyte * 1024
    val gigabyte = megabyte * 1024
    val terabyte = gigabyte * 1024

    return when {
        bytes < kilobyte -> "$bytes B"
        bytes < megabyte -> "%.2f KB".format(bytes.toDouble() / kilobyte)
        bytes < gigabyte -> "%.2f MB".format(bytes.toDouble() / megabyte)
        bytes < terabyte -> "%.2f GB".format(bytes.toDouble() / gigabyte)
        else -> "%.2f TB".format(bytes.toDouble() / terabyte)
    }
}

val videoMaps = mutableMapOf<String, Boolean>()
val imageMaps = mutableMapOf<String, Boolean>()
val audioMaps = mutableMapOf<String, Boolean>()
val documentMaps = mutableMapOf<String, Boolean>()
val archiveMaps = mutableMapOf<String, Boolean>()

fun File.getPretyTime(): String {
    return PrettyTime().format(Date(lastModified()))
}
fun File.isArchive(): Boolean {
    try {
        return archiveMaps.getValue(".$extension")
    } catch (e: Exception) {
    }
    return false
}


fun File.isDocument(): Boolean {
    try {
        return documentMaps.getValue(".$extension")
    } catch (e: Exception) {
    }
    return false
}

fun File.isAudio(): Boolean {
    try {
        return audioMaps.getValue(".$extension")
    } catch (e: Exception) {
    }
    return false
}

fun File.isVideo(): Boolean {
    try {
        return videoMaps.getValue(".$extension")
    } catch (e: Exception) {
    }
    return false
}


fun File.isPhoto(): Boolean {

    try {
        return imageMaps.getValue(".$extension")
    } catch (e: Exception) {
    }
    return false
}