
# Set up

Thêm vô despendence
```
    dependencyResolutionManagement {
		repositoriesMode.set(RepositoriesMode.FAIL_ON_PROJECT_REPOS)
		repositories {
			mavenCentral()
			maven { url 'https://jitpack.io' }
		}
	}



    android {
    
        buildFeatures{
            //noinspection DataBindingWithoutKapt
            dataBinding = true
            viewBinding = true
        }
   
    }


    dependencies {
        //...
	    implementation 'com.gitlab.huyld2:com.taymay.file.viewer:Tag'
	}

```
[link jitpack](https://jitpack.io/#com.gitlab.huyld2/com.taymay.file.viewer/5a729f7865)

# Sử dụng

```
    //files: list file muốn hiển thị
    //position: vị trí hiển thị
    openFilesViewer(files,position){ activity, llTopBanner, llBottomBanner ->

    }
```