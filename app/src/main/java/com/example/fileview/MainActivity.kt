package com.example.fileview

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.example.file_viewer.openFilesViewer
import com.taymay.file.transfer.activity.openFilePicker
import com.taymay.file.transfer.utils.askFilePermistion


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        askFilePermistion(this) {

            openFilePicker { activity, files, fileSize ->
                Log.e("openFilePicker", files.size.toString())
//                elog("file về rồi nè", files.size, fileSize)
                activity.finish()
                openFilesViewer(files,5){ activity, llTopBanner, llBottomBanner ->

                }
            }
        }
    }
}